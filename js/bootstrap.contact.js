$(document).ready(function(){   
		var validator = $("#contact-form").validate({
				errorClass:'has-error',
				validClass:'has-success',
				errorElement:'div',
				highlight: function (element, errorClass, validClass) {
						$(element).closest('.form-control').addClass(errorClass).removeClass(validClass);
				},
				unhighlight: function (element, errorClass, validClass) {
						$(element).parents(".has-error").removeClass(errorClass).addClass(validClass);
				},
				rules: {
						naam: {
								required: true,
								minlength: 2
						},
						email: {
								required: true,
								email: true
						},
						bedrijfsnaam: {
								required: true,
								minlength: 2
						},
						message: {
								required: true,
								minlength: 10
						},
						naam: {
								required: true,
								minlength: 2,
						},
						telefoon: {
								required: true,
								minlength: 3
						},
						vraag: {
								required: true,
								minlength: 10
						} 
				},
				messages: {
						naam: {
								required: '<span class="help-block">Vul je naam in.</span>',
								minlength: jQuery.format('<span class="help-block">Je naam moet minimaal {0} karakters lang zijn.</span>')
						},
						bedrijfsnaam: {
								required: '<span class="help-block">Vul je bedrijfsnaam in.</span>',
								minlength: jQuery.format('<span class="help-block">Je naam moet minimaal {0} karakters lang zijn.</span>')
						},
						email: {
								required: '<span class="help-block">Geef een geldig e-mail adres.</span>',
								email: '<span class="help-block">Vul een geldig e-mail adres in.</span>'
						},
					   
						message: {
								required: '<span class="help-block">Je moet een bericht intypen.</span>',
								minlength: jQuery.format('<span class="help-block">Je moet een bericht intypen.</span>')
						},
						telefoon: {
								required: '<span class="help-block">Vul je telefoon in.</span>',
								minlength: jQuery.format('<span class="help-block">Je telefoonnummer moet minimaal {10} karakters lang zijn.</span>')
						},
						vraag: {
								required: '<span class="help-block">Je moet een bericht intypen.</span>',
								minlength: jQuery.format('<span class="help-block">Je bericht moet minimaal {10} karakters lang zijn.</span>')
						}
				}
		});


		var validator = $("#small-form").validate({
				errorClass:'has-error',
				validClass:'has-success', 
				errorElement:'div',
				highlight: function (element, errorClass, validClass) {
						$(element).closest('.form-control').addClass(errorClass).removeClass(validClass);
				},
				unhighlight: function (element, errorClass, validClass) {
						$(element).parents(".has-error").removeClass(errorClass).addClass(validClass);
				},
				rules: {
						naam: {
								required: true,
								minlength: 2
						},
						bedrijfsnaam: {
								required: true
						},
						
						telefoon: {
								required: true
						}
				},
				messages: {
						naam: {
								required: '<span class="help-block">Vul je naam in.</span>',
								minlength: jQuery.format('<span class="help-block">Je naam moet minimaal {0} karakters lang zijn.</span>')
						},
						bedrijfsnaam: {
								required: '<span class="help-block">Geef je bedrijfsnaam op.</span>'
						},
					   
						telefoon: {
								required: '<span class="help-block">Vul je telefoonnummer in.</span>'
						}
				}
		});

});